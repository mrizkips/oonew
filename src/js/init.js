$(function () {
  $(".rating").rateYo({
    normalFill: "#adb5bd",
    ratedFill: "#F5811E",
    starWidth: "16px",
    spacing: "4px"
  });
  $(".rating-lg").rateYo({
    normalFill: "#adb5bd",
    ratedFill: "#F5811E",
    starWidth: "18px",
    spacing: "4px"
  });
  $(".rating-sm").rateYo({
    normalFill: "#adb5bd",
    ratedFill: "#F5811E",
    starWidth: "14px",
    spacing: "4px"
  });
  $('.carousel').carousel({
    interval: false
  });
});
