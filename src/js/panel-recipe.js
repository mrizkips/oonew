$('.card-recipe-book-panel').click(function() {
  var card = $(this),
  id = card.attr('id');

  if (!card.is('.selected')) {
    $('#recipePanelAction').slideDown();
    $('.card-recipe-book-panel').removeClass('selected');
    card.addClass('selected');

    $('.card-recipe-book-select').fadeOut('fast');
    $('.card-recipe-book-select[data-parent="#'+id+'"]').fadeIn('fast');
  }
});
